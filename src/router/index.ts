import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'

const routes: Array<RouteRecordRaw> = [
	{
		path: '/Login',
		name: 'Login',
		component: {
			default: () => import('../views/login/Login.vue'),
			Tabbar: () => import('../components/Tabbar.vue')
		}
	},
	{
		path: '/',
		name: 'Home',
		components: {
			default: () => import('../views/home/Home.vue'),
			Tabbar: () => import('../components/Tabbar.vue')
		}
	},
	{
		path: '/test',
		name: 'test',
		component: () => import('../views/test.vue')
	},
	{
		path: '/goods',
		name: 'goods',
		component: () => import('../views/goods/index.vue')
	},
	{
		path: '/category',
		name: 'category',
		components: {
			default: () => import('../views/category/index.vue'),
			Tabbar: () => import('../components/Tabbar.vue')
		}
	},
		{
		path: '/search',
		name: 'search',
		components: {
			default: () => import('../views/search/index.vue')
		}
	},
	{
		path: '/a',
		name: 'a',
		component: () => import('../views/test/a.vue')
	},
	{
		path: '/b',
		name: 'b',
		component: () => import('../views/test/b.vue')
	},
	{
		path: '/c',
		name: 'c',
		component: () => import('../views/test/c.vue')
	},
		{
		path: '/me',
		name: 'me',
		component: () => import('../views/me/index.vue')
	}
]

const router = createRouter({
	history: createWebHashHistory(),
	routes
})

export default router
