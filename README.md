<p align="center">
<a href='https://gitee.com/pipepandafeng/blog_vuecode'><img src='https://gitee.com/pipepandafeng/blog_vuecode/widgets/widget_3.svg' alt='Fork me on Gitee'></img></a>
</p>
<p align="center">
<!-- <img src="./docs/.vuepress/public/img/panda4.png" width="300"><br> -->
<a href='https://gitee.com/pipepandafeng/blog_vuecode/stargazers'><img src='https://gitee.com/pipepandafeng/blog_vuecode/badge/star.svg?theme=dark' alt='star'></img><img src='https://gitee.com/pipepandafeng/blog_vuecode/badge/fork.svg?theme=dark' alt='fork'></img></a>
<p align="center">✨ 一款基于 vue3+ts+vite+vant 开发的购物 H5 App，涵盖了购物 App 的常见功能 ✨</p>
</p>

🎉 Tech Stack

- [Vue 3](https://v3.vuejs.org/) - Focus on the content while having the power of HTML and Vue components whenever needed
- [Vite](https://vitejs.dev) - An extremely fast frontend tooling
- [Vant](https://vant-contrib.gitee.io/vant/) - Mobile UI Components built on Vue
- [Typescript](https://www.tslang.cn) -TypeScript saves you time catching errors and providing fixes before you run code.
- [Tailwindcss](https://tailwindcss.com/) - A utility-first CSS framework packed
- [VueUse](https://vueuse.org) family - [`@vueuse/core`](https://github.com/vueuse/vueuse), [`@vueuse/head`](https://github.com/vueuse/head), [`@vueuse/motion`](https://github.com/vueuse/motion), etc.

🦄 Usage

> 本地运行

- `npm install`
- `npm start` 或者 `npm run dev`

> 打包

- `npm run build`

🍞 TODO
- ☑️ vant组件按需加载
- [ ] vant主题自定义
- [ ] 页面跳转动画效果
- [ ] 完成后端接口对接
- [ ] 完成后端爬取购物网站数据，填充至APP
- [ ] 优化登录，注册界面
- [ ] 编写docker文件
- [ ] 打包代码混淆，`UglifyJsPlugin`
- [ ] 部署至`gitee`,`github`
- [ ] 后续开发 `react`，`react native`，`微信小程序`,`android`

🌈 Site

部分页面截图，项目任在紧张开发中...

| ![](./screenshot/Screenshot_1.png)  | ![](./screenshot/Screenshot_2.png)  | ![](./screenshot/Screenshot_3.png)  | ![](./screenshot/Screenshot_4.png)  |
| :---------------------------------: | :---------------------------------: | :---------------------------------: | :---------------------------------: |
| ![](./screenshot/Screenshot_5.png)  | ![](./screenshot/Screenshot_6.png)  | ![](./screenshot/Screenshot_7.png)  | ![](./screenshot/Screenshot_8.png)  |
| ![](./screenshot/Screenshot_9.png)  | ![](./screenshot/Screenshot_10.png) | ![](./screenshot/Screenshot_11.png) | ![](./screenshot/Screenshot_12.png) |
| ![](./screenshot/Screenshot_13.png) | ![](./screenshot/Screenshot_14.png) | ![](./screenshot/Screenshot_17.png) | ![](./screenshot/Screenshot_18.png) |
| ![](./screenshot/Screenshot_15.png) | ![](./screenshot/Screenshot_19.png) | ![](./screenshot/Screenshot_20.png) | ![](./screenshot/Screenshot_21.png) |
| ![](./screenshot/Screenshot_22.jpg) | ![](./screenshot/Screenshot_23.jpg) | ![](./screenshot/Screenshot_24.jpg) | ![](./screenshot/Screenshot_25.jpg) |
| ![](./screenshot/Screenshot_26.jpg) | ![](./screenshot/Screenshot_27.jpg) |                                     |                                     |

🧱 Contribute

<h3>欢迎大家在评论区留下自己的意见，博主会第一时间回答。</h3>
